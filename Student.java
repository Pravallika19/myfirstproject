package com.hcl.variable;

public class Student extends Object {//pojo
	
	private int sid;
	private String name;
	private double percent;
	
	public Student() {
		
		
	}
	public Student(int sid, String name, double percent) {
		super();
		this.sid = sid;
		this.name = name;
		this.percent = percent;
	}
	public int getSid() {
		return sid;
	}
	public void setSid(int sid) {
		this.sid = sid;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getPercent() {
		return percent;
	}
	public void setPercent(double percent) {
		this.percent = percent;
	}
	
	
	
	
	
	
	
	

}
