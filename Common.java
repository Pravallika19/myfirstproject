package com.hcl.codes;

interface Common {
	String markAttendance();

	String dailyTask();

	String displayDetails();
}

class Employee implements Common {
	public String markAttendance() {
		return "Attendance marked for today";
	}

	public String dailyTask() {
		return "Complete coding of module 1";
	}

	public String displayDetails() {
		return "Employee details";
	}

}

class Manager implements Common {
	public String markAttendance() {
		return "Attendance marked for today";
	}

	public String dailyTask() {
		return "Create project architecture";
	}

	public String displayDetails() {
		return "Manager details";
	}

	public class Interface {
		public static void main(String[] args) {

			Employee emp = new Employee();
			System.out.println(emp.markAttendance());
			System.out.println(emp.dailyTask());
			System.out.println(emp.displayDetails());

			Manager m = new Manager();
			System.out.println(m.markAttendance());
			System.out.println(m.dailyTask());
			System.out.println(m.displayDetails());
		}
	}

}