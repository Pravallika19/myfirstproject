package com.hcl.codes;
import java.util.Scanner;
public class DayThreeA {
	    
	    static void printTable(int n){
	    	for(int i =1;i<=10;i++) {
	    		System.out.println(n*i);
	    	}	
	    }

	public static void main(String[] args) {

	    Scanner in = new Scanner(System.in);
	    System.out.println("Enter number: ");
        int n = in.nextInt();

	printTable(n);

	}

}
