package com.hcl.codes;

public class Account {
	String customerName;
	int accNo;
	public Account(String a,int b) {
		a = customerName;
		b = accNo;
	}
	void display() {
		System.out.println("Customer Name:"+customerName);
		System.out.println("Account Number:"+accNo);
	}
}	
	class CurrentAcc extends Account{
		int currentBal;
	    public CurrentAcc(String a, int b, int c){
	    	super(a,b);
	        currentBal=c;
	    }
	    void display() {
	                super.display();
	                System.out.println ("Current Balance:"+currentBal);
	    }
}
	
	class AccDetails extends CurrentAcc{
		int deposit, withdrawal;
	    	public AccDetails(String a, int b, int c,int d,int e) {
			super(a, b, c);
			deposit = d;
			withdrawal = e;
		}
	    	void display(){
	               super.display();
	               System.out.println ("Deposit:"+deposit);
	               System.out.println ("Withdrawals:"+withdrawal);
	    }	    	
}
class Inheritance{
	public static void main(String[] args) {
		AccDetails Acc = new AccDetails("Pravallika",5555,12000,3000,1000);
		Acc.display();
	}
}

