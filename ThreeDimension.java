package com.hcl.codes;

public class ThreeDimension {

	    double width,height,depth;

        public ThreeDimension(double w, double h, double d)
	    {
        	width = w;
        	height = h;
        	depth = d;
	    }
	    public ThreeDimension(double length)
	    {
	    	height=width=depth = length;
	    }
	    public ThreeDimension()
	    { 
	    	height=width=depth = 0;
	    } 

	    double volume()
	    {
	        return width * height * depth;
	    }

public class CodeThree{

	public static void main(String[] args) {
		ThreeDimension shape1 = new ThreeDimension(2,4,6);
		ThreeDimension shape2 = new ThreeDimension(5);
		ThreeDimension shape3 = new ThreeDimension();
		
    double volume;
	    volume = shape1.volume();
	    System.out.println("Volume of shape1 is: " +volume);
		volume = shape2.volume();
		System.out.println("Volume of shape2 is: " +volume);
		volume = shape3.volume();
		System.out.println("Volume of shape3 is: " +volume);	
	
    

	}
}
}
