package com.hcl.inputs;
import java.util.Scanner;
public class Input {
	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		System.out.println("enter your name: ");
		String input = scan.next();
		scan.nextLine();
		System.out.println("thanks "+input);
		
		System.out.println("more details: ");
		String input1 = scan.nextLine();
		System.out.println("about you: "+input1);
	}

}
