package com.hcl.loops;

public class Second {
	public static void main(String[] args) {
		
		int value=-15;
		if(value>0) {
			System.out.println("positive");
		}
		else if(value<0) {
			System.out.println("negative");
		}
		else {
			System.out.println("zero");
		}
		
		for(int i=0;i<10;i++) {
			System.out.println("hi "+i);
		}
		
		int val = 5;
		while (val!=0) {
			System.out.println("hello "+val);
			val--;
		}
		
		int num = 1;
		do {
			System.out.println("welcome "+num);
			num++;
		} while (num>=10);
		
		int arr[]= {12,24,17,46};
		for(int j:arr) {
			System.out.println(j);
		}
	}

}
