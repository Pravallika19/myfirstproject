package com.hcl.codes;

public class Vehicle {
	void noOfWheels() {
		System.out.println("undefined wheels");
	}
}
	
	class Scooter extends Vehicle{
		void noOfWheels() {
			System.out.println("no of Wheels 2");
	}
}
		
	class Car extends Vehicle{
		void noOfWheels() {
			System.out.println("no of Wheels 4");
		}
	}

class Main {

	  public static void main(String[] args) {
		  Vehicle v = new Vehicle();
		  v.noOfWheels();
		  Scooter s = new Scooter();
		  s.noOfWheels();
		  Car c = new Car();
		  c.noOfWheels();
	  }
}