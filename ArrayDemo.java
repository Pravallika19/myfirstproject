package com.hcl.arrays;

public class ArrayDemo {

	public static void main(String[] args) {
		
		int arr[][] = new int[3][];
		System.out.println(arr[0]);
		int arr1[][] = new int[4][3];
		System.out.println(arr1[0]);
		System.out.println(arr1[0][0]);
		arr1[0][0] = 1;
		arr1[1][1] = 2;
		arr1[2][2] = 3;
		arr1[3][0] = 4;
		
		System.out.println(arr1[0][0]);
		System.out.println(arr1[1][1]);
		System.out.println(arr1[2][1]);
		System.out.println(arr1[3][2]);
		
		arr[0] = new int[2];
		arr[1] = new int[2];
		System.out.println(arr[0][1]);
		arr[0][0] = 12;
		System.out.println(arr[0][0]);

	}

}
